import time
import asyncio
import os
import ssl
import sys
from aioudp import open_local_endpoint
from dnslib import DNSRecord
import struct
import logging

CLOUDFLARE = ('1.1.1.1', 853)


class Cache(object):
    """
    Cache Request Configuration
    NOTE:
        Plans to enable DNS Request Caching as well in a future version.
    """
    color_id = 0
    request_id = 0
    clr = "\033[0m"
    colors = (
        "\033[0m",      # RST
        "\033[31m",     # RED
        "\033[92m",     # GREEN
        "\033[33m",     # YELLOW
        "\033[34m",     # BLUE
        "\033[35m",     # MAGENTA
        "\033[36m",     # CYAN
        "\033[37m",     # WHITE
        "\033[95m",     # VIVID
    )

    def __init__(self):
        Cache.color_id += 1
        Cache.request_id += 1
        if Cache.color_id not in range(1, len(Cache.colors)):
            Cache.color_id = 1

        self.color_id = Cache.color_id
        self.request_id = Cache.request_id

    def color(self):
        return self.colors[self.color_id]

    def clr(self):
        return self.colors[0]


async def tcp_read(reader):
    size, = struct.unpack('!H', await reader.readexactly(2))
    data = await reader.readexactly(size)
    return data


async def tcp_write(writer, data):
    size = struct.pack('!H', len(data))
    writer.write(size)
    writer.write(data)
    await writer.drain()


async def forward_query(c):
    """
    Forward DNSRecord Query to Cloudflare
    """
    ssl_context = ssl.create_default_context(ssl.Purpose.SERVER_AUTH)
    ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS)
    ssl_context.verify_mode = ssl.CERT_REQUIRED
    ssl_context.check_hostname = True
    ssl_context.load_verify_locations('./cloudflare.pem')

    await asyncio.sleep(float(os.getenv('DELAY_REQ', 0.1)))

    reader, writer = await asyncio.open_connection(
        *CLOUDFLARE, ssl=ssl_context)
    await tcp_write(writer, c.query_data)
    c.reply_data = await tcp_read(reader)
    c.reply_parse = DNSRecord.parse(c.reply_data)

    # Close Conn with CloudFlare
    writer.close()
    await writer.wait_closed()


async def handle_tcp_request(reader, writer):
    """
    Handle TCP DNS Query
    """
    # Start caching this request into an object
    c = Cache()
    c.start = time.perf_counter()
    addr = writer.get_extra_info('peername')
    c.host = addr[0]
    c.port = addr[1]

    # Read TCP DNS Packet
    c.query_data = await tcp_read(reader)
    c.query_parse = DNSRecord.parse(c.query_data)
    c.qname = c.query_parse.q.get_qname()

    logging.info(
        c.color() +
        f"[{c.host}:{c.port} (tcp)] asking for {str(c.qname)}" +
        c.clr())
    logging.debug(
        c.color() +
        f"[{c.host}:{c.port} (tcp)] Query Data: >\n {c.query_data}" +
        c.clr())
    logging.debug(
        c.color() +
        f"[{c.host}:{c.port} (tcp)] Query DNS: >\n {c.query_parse}" +
        c.clr())

    # Query Cloudflare
    await forward_query(c)
    logging.debug(
        c.color() +
        f"[{c.host}:{c.port} (tcp)] Response Data: >\n {c.reply_data}" +
        c.clr())
    logging.debug(
        c.color() +
        f"[{c.host}:{c.port} (tcp)] Response DNS: >\n {c.reply_parse}" +
        c.clr())

    # Respond to Client
    await tcp_write(writer, c.reply_data)
    writer.close()
    await writer.wait_closed()
    c.elapsed = time.perf_counter() - c.start
    logging.info(
        c.color() +
        f"[{c.host}:{c.port} (tcp)] > replied in {c.elapsed:0.2f} seconds" +
        c.clr())


async def tcp_server(host, port):
    stream = await asyncio.start_server(handle_tcp_request, host, port)
    addr = stream.sockets[0].getsockname()
    logging.info(f'Serving TCP on {addr[0]}:{addr[1]}')
    async with stream:
        await stream.serve_forever()


async def udp_server(address, port):
    """
    Async UDP Server
    NOTE:
        Using aioudp.py
        URL: https://gist.github.com/vxgmichel/e47bff34b68adb3cf6bd4845c4bed448
    """

    logging.info(f'Serving UDP on {address}:{port}')
    endpoint = await open_local_endpoint(host=address, port=port, queue_size=0)

    while True:
        # Start caching this request into an object
        c = Cache()

        # Read UDP DNS Packet
        c.query_data, (c.host, c.port) = await endpoint.receive()
        s = time.perf_counter()
        c.query_parse = DNSRecord.parse(c.query_data)
        c.qname = c.query_parse.q.get_qname()
        logging.info(
            c.color() +
            f"[{c.host}:{c.port} (udp)] asking for {str(c.qname)}" +
            c.clr())
        logging.debug(
            c.color() +
            f"[{c.host}:{c.port} (udp)] Query Data: >\n {c.query_data}" +
            c.clr())
        logging.debug(
            c.color() +
            f"[{c.host}:{c.port} (udp)] Query DNS: >\n {c.query_parse}" +
            c.clr())

        # Query Cloudflare
        await forward_query(c)
        logging.debug(
            c.color() +
            f"[{c.host}:{c.port} (udp)] Response Data: >\n {c.reply_data}" +
            c.clr())
        logging.debug(
            c.color() +
            f"[{c.host}:{c.port} (udp)] Response DNS: >\n {c.reply_parse}" +
            c.clr())

        # Respond to client
        endpoint.send(c.reply_data, (c.host, c.port))
        elapsed = time.perf_counter() - s
        logging.info(
            c.color() +
            f"[{c.host}:{c.port} (udp)] > replied in {elapsed:0.2f} seconds" +
            c.clr())


async def serve(host='127.0.0.1', port=53):
    await asyncio.gather(
        tcp_server(address, port),
        udp_server(address, port),
    )

if __name__ == "__main__":
    port = int(os.getenv('LISTEN_PORT', 53))
    address = os.getenv('LISTEN_ADDR', "0.0.0.0")
    log_level = os.getenv('LOG_LEVEL', "INFO")

    logging.basicConfig(
        format='%(asctime)s %(levelname)s %(message)s',
        level=log_level,
        stream=sys.stdout)

    asyncio.run(serve(address, port))
