FROM python:alpine

WORKDIR /app

# Add files
ADD app .

# Install requirements
RUN apk update
RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 53/tcp
EXPOSE 53/udp

CMD ["python", "proxy.py"]