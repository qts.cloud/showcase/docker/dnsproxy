# DNS TLS Proxy

* Purpose:
  * By default, DNS is sent over a plaintext connection. DNS over TLS is one way to send DNS queries over an encrypted connection.
  * This proxy accepts incomming requests over TCP and UDP and forwards those requests to Cloudflare DNS over TLS connection.

* Notes:
  * Added color-coding to make async and multi-threading more visible.
  * UDP has queue and runs async but it doesn't run multi-threadeded atm. I am using `aioudp` for the UDP implementation.
  * I plan adding caching features in a future version.
  * It's probably worth taking a look at DNS over HTTPS
  * Noticed some potential issues when `DELAY_REQ` is set to `0` and high traffic from multiple clients. I would recommend leaving the config as is.

* Security Concerns:
  * Client <-> Proxy traffic is still happening unencrypted
  * There is no implementation protecting for DNS amplification attacks.
  * This proxy could be potentially used for DNS Poisoning if breached.

* Usecase:
  * In a workplace/home environment, this could be used to mask DNS queries for anybody who might listen (ISP Provider, bad actors)
  * In a Kubernetes environment, it could be used as a sidecar or similar as above, as a border DNS resolver.

* Setup:
  * connect to your `docker` machine and create your work directory

    ```sh
    ssh docker-vm
    # Preparing structure
    mkdir -p qts && cd qts
    git clone https://gitlab.com/qts.cloud/showcase/docker/dnsproxy.git
    cd dnsproxy
    ```

  * Check docker-compose.yml and modify if needed

    ```yaml
    version: "3"

    services:
    dnsproxy:
        image: ibacalu/dnsproxy:latest
        environment:
        LISTEN_PORT: "53"
        LISTEN_ADDR: "0.0.0.0"
        LOG_LEVEL: "INFO"
        DELAY_REQ: 0.1
        restart: always
        ports:
        - 53:53/tcp
        - 53:53/udp
    ```

  * Start dnsproxy

    ```sh
    # Launch container
    docker-compose up -d                # Run the container as a daemon
    docker-compose logs -f dnsproxy      # Follow the logs
    [...]
    Creating dnsproxy_dnsproxy_1 ... done
    Attaching to dnsproxy_dnsproxy_1
    dnsproxy_1  | 2020-08-26 11:01:12,528 INFO Serving UDP on 0.0.0.0:53
    dnsproxy_1  | 2020-08-26 11:01:12,528 INFO Serving TCP on 0.0.0.0:53
    [...]
    ```
